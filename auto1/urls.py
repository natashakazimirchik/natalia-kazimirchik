from django.urls import path
from . import views
from .views import AutoListView, MotoListView
urlpatterns = [
    path('new_ad/', views.new_ad, name='new_ad'),
    path('new_ad_moto/', views.new_ad_moto, name='new_ad_moto'),
    # path('auto/', views.auto, name='auto'),
    path('auto', AutoListView.as_view(), name='auto'),
    path('auto/<int:pk>', views.auto_details, name='auto_details'),
    path('auto/<int:pk>/delete', views.delete_auto, name='delete_auto'),
    path('auto/<int:pk>/share/', views.auto_share, name='auto_share'),
    path('auto/search/', views.auto_search, name='auto_search'),
    # path('moto/', views.moto, name='moto'),
    path('moto', MotoListView.as_view(), name='moto'),
    path('moto/<int:pk>', views.moto_details, name='moto_details'),
    path('moto/<int:pk>/delete', views.delete_moto, name='delete_moto'),
    path('a_filters/', views.auto_filter, name='auto_filters'),
    path('m_filters/', views.moto_filter, name='moto_filters'),
    path('auto/<int:pk>/mark', views.auto_bookmark, name='auto_bookmark'),
    path('a_bookmarks/', views.auto_bookmarks, name='a_bookmarks'),
    path('moto/<int:pk>/mark', views.moto_bookmark, name='moto_bookmark'),
    path('m_bookmarks/', views.moto_bookmarks, name='m_bookmarks'),
    path('all_reviews', views.all_reviews, name='reviews'),
    path('auto/<int:pk>/create/', views.auto_create_review, name='auto_create_review'),
    path('moto/<int:pk>/create/', views.moto_create_review, name='moto_create_review'),
    path('auto/<int:pk>/rew_about_this_car/', views.all_rew_about_this_car, name='all_rew_about_this_car'),
    path('moto/<int:pk>/rew_about_this_motor/', views.all_rew_about_this_motor, name='all_rew_about_this_motor'),
    path('rew_about_this_car/<int:pk>/', views.a_rev_details, name='a_rev_details'),
    path('rew_about_this_motor/<int:pk>/', views.m_rev_details, name='m_rev_details'),
    path('rew_about_this_car/<int:pk>/delete', views.a_rev_delete, name='a_rev_delete'),
    path('rew_about_this_motor/<int:pk>/delete', views.m_rev_delete, name='m_rev_delete'),

]

