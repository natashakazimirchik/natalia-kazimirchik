from django.contrib import admin
from .models import *

@admin.register(Auto)
class AutoAdmin(admin.ModelAdmin):
    list_display = ('brand', 'model', 'price', 'published_date')
    search_fields = ('brand',)


@admin.register(Moto)
class MotoAdmin(admin.ModelAdmin):
    list_display = ('brand', 'model', 'price', 'published_date')
    search_fields = ('brand',)

@admin.register(AutoReviews)
class AutoReviewsAdmin(admin.ModelAdmin):
    list_display = ('author', 'reviewed', 'rate')

@admin.register(MotoReviews)
class MotoReviewsAdmin(admin.ModelAdmin):
    list_display = ('author', 'reviewed', 'rate')

@admin.register(UserAutoMarks)
class UserAutoMarksAdmin(admin.ModelAdmin):
    list_display = ('user', 'auto', 'in_bookmarks')

@admin.register(UserMotoMarks)
class UserMotoMarksAdmin(admin.ModelAdmin):
    list_display = ('user', 'moto', 'in_bookmarks')
