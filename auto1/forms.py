from django import forms
from .models import *


class PostAutoForm(forms.ModelForm):
    class Meta:
        model = Auto
        exclude = ("published_date",)
        widgets = {
            'brand': forms.Select(choices=brand_choice),
            'model': forms.Select(choices=models_choice),
            'type_of_body': forms.TextInput(),
            'transmission': forms.TextInput(),
            'drive_unit': forms.TextInput(),
            'type_of_fuel': forms.TextInput(),
            'year': forms.TextInput(),
            'location': forms.TextInput(),
            'mileage': forms.TextInput(),
            'engine_capacity': forms.TextInput(),
            'description': forms.Textarea(),
            'price': forms.TextInput(),
            'pictures': forms.FileInput(),
        }

    # def __init__(self, *args, **kwargs):
    #     super(PostAutoForm, self).__init__(*args, **kwargs)
    #     for field in iter(self.fields):
    #         self.fields[field].widget.attrs.update({
    #             'class': 'form-control'
    #         })


class PostMotoForm(forms.ModelForm):
    class Meta:
        model = Moto
        exclude = ("published_date",)
        widgets = {

            'brand': forms.TextInput(),
            'model': forms.TextInput(),
            'year': forms.TextInput(),
            'location': forms.TextInput(),
            'mileage': forms.TextInput(),
            'engine_capacity': forms.TextInput(),
            'description': forms.Textarea(),
            'price': forms.TextInput(),
            'pictures': forms.FileInput(),
        }

    # def __init__(self, *args, **kwargs):
    #     super(PostMotoForm, self).__init__(*args, **kwargs)
    #     for field in iter(self.fields):
    #         self.fields[field].widget.attrs.update({
    #             'class': 'form-control'
    #         })


class AutoReviewForm(forms.ModelForm):
    class Meta:
        model = AutoReviews
        fields = ('rate', 'text',)
        widgets = {
            'rate': forms.RadioSelect(attrs={'class': 'inline'}),
            'text': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Твой отзыв'})
        }

    # def __init__(self, *args, **kwargs):
    #     super(AutoReviewForm, self).__init__(*args, **kwargs)
    #     self.fields['rate'].label = 'Рейтинг'
    #     self.fields['text'].label = 'Отзыв'


class MotoReviewForm(forms.ModelForm):
    class Meta:
        model = MotoReviews
        fields = ('rate', 'text',)
        widgets = {
            'rate': forms.RadioSelect(attrs={'class': 'inline'}),
            'text': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Твой отзыв', 'name': 'kjlf'})
        }

    # def __init__(self, *args, **kwargs):
    #     super(MotoReviewForm, self).__init__(*args, **kwargs)
    #     self.fields['rate'].label = 'Рейтинг'
    #     self.fields['text'].label = 'Отзыв'


# def __init__(self, *args, **kwargs):
#     super(AutoReviewForm, self).__init__(*args, **kwargs)
#     self.fields['reviewed'].initial = Auto.objects.all().last()


class EmailPostForm(forms.Form):
    name = forms.CharField(max_length=25, label='Имя', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Ваше Имя'}))
    email = forms.EmailField(label='От кого', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Email отправителя'}))
    to = forms.EmailField(label='Кому', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Email получателя'}))
    comments = forms.CharField(required=False, label='Сообщение', widget=forms.Textarea(
        attrs={'class': 'form-control', 'placeholder': 'Твой комментарий'}))


class SearchForm(forms.Form):
    query = forms.CharField()
    #
    # def __init__(self, *args, **kwargs):
    #     super(SearchForm, self).__init__(*args, **kwargs)
    #     self.fields['query'].label = 'Ваш запрос'