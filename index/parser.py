import requests
from bs4 import BeautifulSoup as BS

r = requests.get('https://index.minfin.com.ua/reference/coronavirus/geography/belarus/')
html = BS(r.text, 'html.parser')
body = html.html.table


class CoronaStat:
    infected = body.find('strong', {'class': 'gold'})
    deaths = body.find('strong', {'class': 'red'})
    recovered = body.find('strong', {'class': 'green'})
    sick_now = body.find('strong', {'class': 'blue'})
    tests_done = body.find('strong', {'class': 'teal normal'})

    def get_infected(self):
        return self.infected.getText()

    def get_deaths(self):
        return self.deaths.getText()

    def get_recovered(self):
        return self.recovered.getText()

    def get_sick_now(self):
        return self.sick_now.getText()

    def get_tests_done(self):
        return self.tests_done.getText()

# st = body.find_all('strong', {'class': 'gold'})

# for t in st:
#     qt = t.getText()
