from django.urls import path, include
from .views import index, currenc

urlpatterns = [
    path('', index, name="index"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('currency/', currenc, name="currenc")
    ]
