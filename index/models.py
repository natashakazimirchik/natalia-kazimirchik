from django.db import models
from datetime import datetime


# Create your models here.
class Currency(models.Model):
    Cur_Abbreviation = models.CharField(max_length=10)
    Cur_Scale = models.PositiveIntegerField(null=True, blank=True)
    Cur_Name = models.CharField(max_length=200, null=True, blank=True)
    Cur_OfficialRate = models.DecimalField(max_digits=14, decimal_places=5, null=True, blank=True)
    date = models.DateTimeField(default=datetime.now, blank=True)


    def __str__(self):
        return self.Cur_Name
