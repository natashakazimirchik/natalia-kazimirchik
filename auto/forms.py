from django import forms
from .models import *


class PostAutoForm(forms.ModelForm):
    class Meta:
        model = Auto
        fields = (
            'brand',
            'model',
            'type_of_body',
            'transmission',
            'drive_unit',
            'type_of_fuel',
            'year',
            'location',
            'mileage',
            'engine_capacity',
            'description',
            'price',
            'pictures',
            )
        widgets = {

                'brand': forms.Select(choices=brand_choice),
                'model': forms.Select(choices=models_choice),
                'type_of_body': forms.TextInput(attrs={'class': 'form-control'}),
                'transmission': forms.TextInput(attrs={'class': 'form-control'}),
                'drive_unit': forms.TextInput(attrs={'class': 'form-control'}),
                'type_of_fuel': forms.TextInput(attrs={'class': 'form-control'}),
                'year': forms.TextInput(attrs={'class': 'form-control'}),
                'location': forms.TextInput(attrs={'class': 'form-control'}),
                'mileage': forms.TextInput(attrs={'class': 'form-control'}),
                'engine_capacity': forms.TextInput(attrs={'class': 'form-control'}),
                'description': forms.Textarea(attrs={'class': 'form-control'}),
                'price': forms.TextInput(attrs={'class': 'form-control'}),
                'pictures': forms.FileInput(attrs={'class': 'form-control'}),
            }


class PostMotoForm(forms.ModelForm):
    class Meta:
        model = Moto
        fields = (
            'brand',
            'model',
            'year',
            'location',
            'mileage',
            'engine_capacity',
            'description',
            'price',
            'pictures',
            )
        widgets = {

            'brand':forms.TextInput(attrs={'class':'form-control'}),
            'model':forms.TextInput(attrs={'class':'form-control'}),
            'year':forms.TextInput(attrs={'class':'form-control'}),
            'location':forms.TextInput(attrs={'class':'form-control'}),
            'mileage':forms.TextInput(attrs={'class':'form-control'}),
            'engine_capacity':forms.TextInput(attrs={'class':'form-control'}),
            'description':forms.Textarea(attrs={'class':'form-control'}),
            'price':forms.TextInput(attrs={'class':'form-control'}),
            'pictures':forms.FileInput(attrs={'class':'form-control'}),

        }

class AutoReviewForm(forms.ModelForm):
    class Meta:
        model = AutoReviews
        fields = (
            'rate',
            'text',
        )
        widgets = {
            'rate': forms.RadioSelect(attrs={'class': 'inline'}),
            'text': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Твой отзыв', 'name': 'kjlf'})
        }

class MotoReviewForm(forms.ModelForm):
    class Meta:
        model = MotoReviews
        fields = (
            'rate',
            'text',
        )
        widgets = {
            'rate': forms.RadioSelect(attrs={'class':'inline'}),
            'text': forms.Textarea(attrs={'class':'form-control', 'placeholder':'Твой отзыв','name':'kjlf'})
        }



    # def __init__(self, *args, **kwargs):
    #     super(AutoReviewForm, self).__init__(*args, **kwargs)
    #     self.fields['reviewed'].initial = Auto.objects.all().last()
