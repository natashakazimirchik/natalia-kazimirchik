from django.contrib import admin
from .models import *




admin.site.register(Moto)
admin.site.register(Auto)
admin.site.register(AutoReviews)
admin.site.register(MotoReviews)
admin.site.register(UserAutoMarks)
admin.site.register(UserMotoMarks)