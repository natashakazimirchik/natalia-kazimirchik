from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import *
from .forms import PostAutoForm, PostMotoForm, AutoReviewForm, MotoReviewForm
from django.utils import timezone
from .filters import AutoFilter, MotoFilter


def auto(request):
    cars = Auto.objects.all().order_by('published_date')[::-1]
    context = {
        'cars': cars,
    }
    return render(request, 'auto.html', context)

def auto_details(request, pk):
    car = get_object_or_404(Auto, pk=pk)
    return render(request, 'auto_details.html', {'car': car})


def auto_filter(request):
    a_filter = AutoFilter(request.GET, queryset=Auto.objects.all().order_by('published_date').reverse())
    context = {
        'a_filter': a_filter,
    }
    return render(request, 'auto_filters.html', context)

def moto_filter(request):
    m_filter = MotoFilter(request.GET, queryset=Moto.objects.all().order_by('published_date').reverse())
    context = {
        'm_filter': m_filter,
    }
    return render(request, 'moto_filters.html', context)


def moto (request):
    motors = Moto.objects.all()
    context = {
        'motors': motors,
    }
    return render(request, 'moto.html', context)

def moto_details(request, pk):
    motor = get_object_or_404(Moto, pk=pk)
    return render(request, 'moto_details.html', {'motor': motor})

def new_ad(request):
    if request.method == "POST":
        form = PostAutoForm(request.POST, request.FILES)
        if form.is_valid():
            car = form.save(commit=False)
            car.author = request.user
            car.published_date = timezone.now()
            form.save()
            return redirect('ab:auto')
    else:
        form = PostAutoForm()
    return render(request, 'new_ad.html', {'form': form})


def new_ad_moto(request):
    if request.method == "POST":
        form = PostMotoForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            form.save()
            return redirect('ab:moto')
    else:
        form = PostMotoForm()
    return render(request, 'new_ad.html', {'form': form})


def auto_bookmarks(request):
    cars = [i.auto for i in UserAutoMarks.objects.filter(user=request.user)]
    return render(request, 'a_bookmarks.html', {'cars': cars})


def auto_bookmark(request, pk):
    user = request.user
    car = get_object_or_404(Auto, pk=pk)
    if not UserAutoMarks.objects.filter(user=user, auto=car).first():
        UserAutoMarks.objects.create(user=user, auto=car, in_bookmarks=True)

    return render(request, 'auto_details.html', {'car': car})


def moto_bookmarks(request):
    motors = [i.moto for i in UserMotoMarks.objects.filter(user=request.user)]
    return render(request, 'm_bookmarks.html', {'motors': motors})


def moto_bookmark(request, pk):
    user = request.user
    motor = get_object_or_404(Moto, pk=pk)
    if not UserMotoMarks.objects.filter(user=user, moto=motor).first():
        UserMotoMarks.objects.create(user=user, moto=motor, in_bookmarks=True)

    return render(request, 'moto_details.html', {'motor': motor})

def all_reviews(request):
    all_a_rew = [i for i in AutoReviews.objects.filter(author=request.user)[::-1]]
    all_m_rew = [i for i in MotoReviews.objects.filter(author=request.user)[::-1]]

    context = {
        'all_a_rew': all_a_rew,
        'all_m_rew': all_m_rew
        }
    return render(request, 'all_reviews.html', context)

def auto_create_review(request, pk):
    car = get_object_or_404(Auto, pk=pk)
    if request.method == "POST":
        form = AutoReviewForm(request.POST)
        if form.is_valid():
            rew = form.save(commit=False)
            rew.author = request.user
            rew.reviewed = car
            form.save()
            return redirect('ab:auto_details', pk=car.pk)
    else:
        form = AutoReviewForm()
    return render(request, 'create_review.html', {'form': form})

def moto_create_review(request, pk):
    motor = get_object_or_404(Moto, pk=pk)
    if request.method == "POST":
        form = MotoReviewForm(request.POST)
        if form.is_valid():
            rew = form.save(commit=False)
            rew.author = request.user
            rew.reviewed = motor
            form.save()
            return redirect('ab:moto_details', pk=motor.pk)
    else:
        form = MotoReviewForm()
    return render(request, 'create_review.html', {'form': form})




