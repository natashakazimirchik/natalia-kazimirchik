from django.urls import path
from . import views

urlpatterns = [
    path('new_ad/', views.new_ad, name='new_ad'),
    path('new_ad_moto/', views.new_ad_moto, name='new_ad_moto'),
    path('auto/', views.auto, name='auto'),
    path('auto/<int:pk>', views.auto_details, name='auto_details'),
    path('moto/', views.moto, name='moto'),
    path('moto/<int:pk>', views.moto_details, name='moto_details'),
    path('a_filters/', views.auto_filter, name='auto_filters'),
    path('m_filters/', views.moto_filter, name='moto_filters'),
    path('auto/<int:pk>/mark', views.auto_bookmark, name='auto_bookmark'),
    path('a_bookmarks/', views.auto_bookmarks, name='a_bookmarks'),
    path('moto/<int:pk>/mark', views.moto_bookmark, name='moto_bookmark'),
    path('m_bookmarks/', views.moto_bookmarks, name='m_bookmarks'),
    path('all_reviews', views.all_reviews, name='reviews'),
    path('auto/<int:pk>/create/', views.auto_create_review, name='auto_create_review'),
    path('moto/<int:pk>/create/', views.moto_create_review, name='moto_create_review'),

]
