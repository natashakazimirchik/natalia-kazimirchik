from django.db import models
from django.utils import timezone
from easy_thumbnails.fields import ThumbnailerImageField
from .csv.cars import brand_choice, models_choice
from django.contrib.auth.models import User


class Moto(models.Model):

    class Meta:
        verbose_name = "Motorcycle"
        verbose_name_plural = "Motorcycles"

    brand = models.CharField(max_length=50, default='')
    model = models.CharField(max_length=255, default='')
    year = models.CharField(max_length=10)
    location = models.CharField(max_length=20)
    mileage = models.CharField(max_length=20)
    engine_capacity = models.CharField(max_length=20)
    description = models.TextField(default='')
    price = models.DecimalField(max_digits=9, decimal_places=3)
    published_date = models.DateTimeField(default=timezone.now)
    pictures = ThumbnailerImageField(upload_to='photos', default='photos/noimg.png')

    def __str__(self):
        return f'{self.brand} - {self.model}'


class Auto(models.Model):

    class Meta:
        verbose_name = "Car"
        verbose_name_plural = "Cars"

    brand = models.CharField(max_length=50, choices=brand_choice)
    model = models.CharField(max_length=255, choices=models_choice)
    transmission = models.CharField(max_length=20)
    drive_unit = models.CharField(max_length=20)
    type_of_fuel = models.CharField(max_length=20)
    type_of_body = models.CharField(max_length=20)
    year = models.CharField(max_length=10)
    location = models.CharField(max_length=20)
    mileage = models.CharField(max_length=20)
    engine_capacity = models.CharField(max_length=20)
    description = models.TextField(default='')
    price = models.DecimalField(max_digits=9, decimal_places=3)
    published_date = models.DateTimeField(default=timezone.now)
    pictures = ThumbnailerImageField(upload_to='photos', default='photos/noimg.png')

    def __str__(self):
        return f'{self.brand} - {self.model}'


class AutoReviews(models.Model):

    RATE_CHOICE = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    reviewed = models.ForeignKey(Auto, on_delete=models.CASCADE, related_name='review')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    rate = models.PositiveSmallIntegerField(choices=RATE_CHOICE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.text


class MotoReviews(models.Model):
    RATE_CHOICE = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    reviewed = models.ForeignKey(Moto, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    rate = models.PositiveSmallIntegerField(choices=RATE_CHOICE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.text


class UserAutoMarks(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    auto = models.ForeignKey(Auto, on_delete=models.CASCADE)
    in_bookmarks = models.BooleanField(default=False)

    def bookmarks(self):
        self.in_bookmarks = True
        self.save()

    def __str__(self):
        return f'{self.user.username} - {self.auto.brand} {self.auto.model}'


class UserMotoMarks(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    moto = models.ForeignKey(Moto, on_delete=models.CASCADE)
    in_bookmarks = models.BooleanField(default=False)

    def bookmarks(self):
        self.in_bookmarks = True
        self.save()

    def __str__(self):
        return f'{self.user.username} - {self.moto.brand} {self.moto.model}'



# class Rating(models.Model):
#
#     reviewed = models.ForeignKey(Moto, on_delete=models.CASCADE)
#     rate = models.PositiveSmallIntegerField(default=0,
#         validators=[
#             MaxValueValidator(5),
#             MinValueValidator(0)
#             ]
#     )
#
#     def __str__(self):
#         return str(self.pk)
#
# def this_is_view(request):
#     obj = Rating.object.filter(rate=0).order_by('?').first()
#     context ={
#         'obj':obj
#     }
#     return render(request, 'ratings.html', context)
#
# path('/', views.this_is_view, name='this_is_view')