from django.shortcuts import render, get_object_or_404
from .models import Post, Category
from .forms import PostForm
from taggit.models import Tag
from django.template.defaultfilters import slugify


def home_view(request):
    posts = Post.objects.all()
    common_tags = Post.tags.most_common()[:4]
    context = {
        'posts': posts,
        'common_tags': common_tags,
    }
    return render(request, 'index_news.html', context)


def news_add(request):
    posts = Post.objects.all()
    common_tags = Post.tags.most_common()[:4]
    form = PostForm(request.POST)
    if form.is_valid():
        newpost = form.save(commit=False)
        newpost.slug = slugify(newpost.title)
        newpost.save()
        form.save_m2m()
    context = {
        'posts': posts,
        'common_tags': common_tags,
        'form': form,
    }
    return render(request, 'add.html', context)


def detail_view(request, slug):
    post = get_object_or_404(Post, slug=slug)
    context = {
        'post': post,
    }
    return render(request, 'detail.html', context)


def tagged(request, slug):
    tag = get_object_or_404(Tag, slug=slug)
    common_tags = Post.tags.most_common()[:4]
    posts = Post.objects.filter(tags=tag)
    context = {
        'tag': tag,
        'common_tags': common_tags,
        'posts': posts,
    }
    return render(request, 'home.html', context)


def category_news(request, slug=None):
    category = Category.objects.get(slug=slug)
    posts = Post.objects.filter(category=category)
    context = {
        'category': category,
        'posts': posts,
        'slug': slug
    }
    return render(request, 'category_news.html', context)
