from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from django.urls import reverse
from django.contrib.auth.models import User
from index.models import Currency


class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)
    icon = ThumbnailerImageField(upload_to='content', default='catalog/icon/noimg.png', blank=True)
    picture = models.ImageField(upload_to='content', default='catalog/category/noimg.png', blank=True)
    sort = models.PositiveIntegerField()

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('catalog:category_list',
                       args=[self.slug])


class SubCategory(models.Model):
    category = models.ForeignKey(Category, related_name='Subcategory', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)
    icon = ThumbnailerImageField(upload_to='content', default='catalog/icon/noimg.png', blank=True)
    picture = models.ImageField(upload_to='content', default='catalog/category/noimg.png', blank=True)
    sort = models.PositiveIntegerField()

    class Meta:
        ordering = ('name',)
        verbose_name = 'subcategory'
        verbose_name_plural = 'subcategories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('catalog:subcategory_list',
                       args=[self.slug])


# class ProperyType(models.Model):
#     name = models.CharField(max_length=200, db_index=True, null=True, blank=True)
#     type = models.ForeignKey(SubCategory, on_delete=models.CASCADE, null=True, blank=True)
#
#     def __str__(self):
#         return self.name
#
#
# class PropertyInstance(models.Model):
#     name = models.CharField(max_length=200, db_index=True, null=True, blank=True)
#     type = models.ForeignKey(SubCategory, on_delete=models.CASCADE, null=True, blank=True)
#     value = models.TextField(blank=True)
#
#     def __str__(self):
#         return self.name


class Product(models.Model):
    subcategory = models.ForeignKey(SubCategory, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    icon = ThumbnailerImageField(upload_to='catalog/icon', default='catalog/icon/noimg.png', blank=True)
    picture = models.ImageField(upload_to='catalog/product/%Y/%m/%d', default='catalog/product/noimg.png', blank=True)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True, blank=True)
    stock = models.PositiveIntegerField()
    available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    count_buy = models.PositiveIntegerField()
    rating = models.DecimalField(max_digits=10, decimal_places=2)
    price_usd = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        ordering = ('name',)
        verbose_name = u'товар'
        verbose_name_plural = u'товары'

    def __str__(self):
        return self.name

    def get_absolute_url(self, subcategory):
        return reverse('catalog:product_detail',
                       args=[subcategory.slug, self.slug])


class Reviews(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=True, null=True)
    pub_date = models.DateTimeField(auto_now_add=True)
    content = models.TextField(max_length=200)

    def __str__(self):
        return self.content
